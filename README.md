# OpenML dataset: fifa

https://www.openml.org/d/45012

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The datasets provided include the players data for the Career Mode from FIFA 22.
It only includes male football players with known wage.

The goal is to predict the wage of the player based on his attributes.

**Attribute Description**

The features describe self-explanatory properties and skills of the player.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45012) of an [OpenML dataset](https://www.openml.org/d/45012). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45012/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45012/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45012/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

